package com.sunhf.logkafka.config;


import com.sunhf.logkafka.delivery.AsynchronousDeliveryStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

@EnableConfigurationProperties(LogKafkaProperties.class)
public class LogKafkaAutoConfiguration {

    @Autowired
    private LogKafkaProperties logKafkaProperties;

    @Bean
    public KafkaAppender rollingFileAppender() {
        KafkaAppender appender = new KafkaAppender();
        appender.setTopic(logKafkaProperties.getLogTopic());
        appender.setDeliveryStrategy(new AsynchronousDeliveryStrategy());
        appender.producerConfig.put("bootstrap.servers", logKafkaProperties.getBootstrapServers());
        appender.producerConfig.put("linger.ms", logKafkaProperties.getLingerMs());
        System.out.println("kafkaAppender已加载，配置："+logKafkaProperties);
        appender.start();
        return appender;
    }


}
