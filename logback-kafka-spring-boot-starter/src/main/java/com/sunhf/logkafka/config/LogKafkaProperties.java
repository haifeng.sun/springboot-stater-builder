package com.sunhf.logkafka.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "spring.logkafka")
public class LogKafkaProperties {
    //kafka相关
    private String bootstrapServers = "127.0.0.1:9092";
    private String groupId = "defaultGroup";
    private Integer retries;
    private Integer batchSize;
    private Integer lingerMs;
    private Integer bufferMemory;
    private String keySerializer = "org.apache.kafka.common.serialization.StringSerializer";
    private String valueSerializer = "org.apache.kafka.common.serialization.StringSerializer";

    //logback相关
    private String logfile = "logkafka.log";
    private String logLevel = "INFO";//DEBUG INFO WARN ERROR
    private String fileNameParttern = "${APP_NAME}.%d{yyyy-MM-dd}.log";
    private String logParttern; //日志打印样式
    private Integer maxHistory = 7; //日志最大保留期限

    //logkafka相关
    private String logFormatter; //默认日志格式化类型
    private String logTopic; //日志topic地址
    private Boolean enableFile = false;
    private Boolean enableConsole = false;
    private Boolean enableKafka = true;

    public String getBootstrapServers() {
        return bootstrapServers;
    }

    public void setBootstrapServers(String bootstrapServers) {
        this.bootstrapServers = bootstrapServers;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public Integer getRetries() {
        return retries;
    }

    public void setRetries(Integer retries) {
        this.retries = retries;
    }

    public Integer getBatchSize() {
        return batchSize;
    }

    public void setBatchSize(Integer batchSize) {
        this.batchSize = batchSize;
    }

    public Integer getLingerMs() {
        return lingerMs;
    }

    public void setLingerMs(Integer lingerMs) {
        this.lingerMs = lingerMs;
    }

    public Integer getBufferMemory() {
        return bufferMemory;
    }

    public void setBufferMemory(Integer bufferMemory) {
        this.bufferMemory = bufferMemory;
    }

    public String getKeySerializer() {
        return keySerializer;
    }

    public void setKeySerializer(String keySerializer) {
        this.keySerializer = keySerializer;
    }

    public String getValueSerializer() {
        return valueSerializer;
    }

    public void setValueSerializer(String valueSerializer) {
        this.valueSerializer = valueSerializer;
    }

    public String getLogfile() {
        return logfile;
    }

    public void setLogfile(String logfile) {
        this.logfile = logfile;
    }

    public String getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(String logLevel) {
        this.logLevel = logLevel;
    }

    public String getFileNameParttern() {
        return fileNameParttern;
    }

    public void setFileNameParttern(String fileNameParttern) {
        this.fileNameParttern = fileNameParttern;
    }

    public String getLogParttern() {
        return logParttern;
    }

    public void setLogParttern(String logParttern) {
        this.logParttern = logParttern;
    }

    public String getLogFormatter() {
        return logFormatter;
    }

    public void setLogFormatter(String logFormatter) {
        this.logFormatter = logFormatter;
    }

    public Boolean getEnableFile() {
        return enableFile;
    }

    public void setEnableFile(Boolean enableFile) {
        this.enableFile = enableFile;
    }

    public Boolean getEnableConsole() {
        return enableConsole;
    }

    public void setEnableConsole(Boolean enableConsole) {
        this.enableConsole = enableConsole;
    }

    public Boolean getEnableKafka() {
        return enableKafka;
    }

    public void setEnableKafka(Boolean enableKafka) {
        this.enableKafka = enableKafka;
    }

    public Integer getMaxHistory() {
        return maxHistory;
    }

    public void setMaxHistory(Integer maxHistory) {
        this.maxHistory = maxHistory;
    }

    public String getLogTopic() {
        return logTopic;
    }

    public void setLogTopic(String logTopic) {
        this.logTopic = logTopic;
    }

    @Override
    public String toString() {
        return "LogKafkaProperties{" +
                "bootstrapServers='" + bootstrapServers + '\'' +
                ", groupId='" + groupId + '\'' +
                ", retries=" + retries +
                ", batchSize=" + batchSize +
                ", lingerMs=" + lingerMs +
                ", bufferMemory=" + bufferMemory +
                ", keySerializer='" + keySerializer + '\'' +
                ", valueSerializer='" + valueSerializer + '\'' +
                ", logfile='" + logfile + '\'' +
                ", logLevel='" + logLevel + '\'' +
                ", fileNameParttern='" + fileNameParttern + '\'' +
                ", logParttern='" + logParttern + '\'' +
                ", maxHistory=" + maxHistory +
                ", logFormatter='" + logFormatter + '\'' +
                ", logTopic='" + logTopic + '\'' +
                ", enableFile=" + enableFile +
                ", enableConsole=" + enableConsole +
                ", enableKafka=" + enableKafka +
                '}';
    }
}
