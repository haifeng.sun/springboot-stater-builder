#RocketMQ 4.2 SpringBootStarter

##RocketMQ 4.3新特性
支持感知消息：即消息先通过MQ发送到队列，队列返回响应信息，之后发送方进行数据库事务操作，如果失败或中途宕机RocketMQ自动进行回滚,由于发布时间较短，暂时不使用该特性。


##使用教程
###生产者
springboot启动类加入@EnableRocketMQ注解，启动时会自动加载生产者
```
@SpringBootApplication
@ComponentScan("com.sunhf.transaction.service.impl")
@EnableRocketMQ(consumerInit = false)
public class DubboLanucher {

    public static void main(String[] args) {
        new SpringApplicationBuilder(DubboLanucher.class)
                .web(WebApplicationType.NONE)
                .run(args);

    }
}

```
application.properties示例配置：
以spring.rocketmq为前缀，后面部分与官方配置文件指定的配置名称后缀相同或采用以下格式
例如：
```
spring.rocketmq.default-group=DefaultGroup
spring.rocketmq.namesrv-addr=192.168.2.56:9876;192.168.2.58:9876
```
创建消息发送服务类
```
@Service
public class OrderServiceImpl implements OrderService {

    private static final Logger logger = LoggerFactory.getLogger(OrderServiceImpl.class);

    @Autowired
    private MQProducer producer;

    @Override
    public Long createOrder(Map<String, Object> params) {
        if(params == null || params.isEmpty()) throw new RuntimeException("非法的参数");
        logger.info("假装做了一些逻辑处理,生成了订单,请求参数:"+ JSON.toJSONString(params));
        try {
            Thread.sleep(new Random().nextInt(100)); //模拟延迟
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Message m = new Message();
        m.setBody("测试消息".getBytes());
        m.setTopic("springboot-starter-test");
//        m.setTags("test-single");
        try {
            producer.send(m);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Random().nextLong();
    }
}
```
###消费者
启动类加入@EnableRocketMQ 注解producerInit默认为true，会自动加载生产者,如不需要生产者可将producerInit设置为false
```
@SpringBootApplication
@ComponentScan("com.sunhf.transaction")
@EnableRocketMQ(producerInit = false)
public class RocketMQLanucher {



    public static void main(String[] args) {
        new SpringApplicationBuilder(RocketMQLanucher.class)
                .web(WebApplicationType.NONE)
                .run(args);

    }


}
```
创建消费实例,注意泛型需要遵循javaBean规范，该组件包会自动反序列化消息。除此意外额外支持String类型的消息接收。其他类型暂不支持。
```

@Consumer(consumerGroup = "DefaultGroupx5", topic = "springboot-starter-test", tag = "*")
public class OrderConsumer extends AbstractMQPushConsumer<String> {



    @Override
    public boolean process(String s, Map<String, Object> map) {
        System.out.println(s);
        System.out.println(JSON.toJSONString(map));
        return true;
    }
}

```

