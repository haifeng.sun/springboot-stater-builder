package com.sunhf.rocketmq.annotation;

import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * 消费者实现类
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface Consumer {
    /**
     * 消费组
     * @return
     */
    String consumerGroup() default "DefaultGroup";

    /**
     * 消费topic
     * @return
     */
    String topic();


    String tag() default "*";
}
