package com.sunhf.rocketmq.annotation;

import com.sunhf.rocketmq.config.RocketMQAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({RocketMQRegistrar.class, RocketMQAutoConfiguration.class})
public @interface EnableRocketMQ {


    boolean producerInit() default true;

    boolean consumerInit() default true;

}
