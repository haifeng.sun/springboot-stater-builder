package com.sunhf.rocketmq.annotation;

import com.sunhf.rocketmq.config.ConsumerPostProcessor;
import com.sunhf.rocketmq.config.ProducerAutoConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

import java.util.Map;

public class RocketMQRegistrar implements ImportBeanDefinitionRegistrar {

    private static final Logger logger = LoggerFactory.getLogger(RocketMQRegistrar.class);

    @Override
    public void registerBeanDefinitions(AnnotationMetadata annotationMetadata, BeanDefinitionRegistry beanDefinitionRegistry) {
        if(annotationMetadata.hasAnnotation(EnableRocketMQ.class.getName())){
            //获取EnableEcho注解的所有属性的value
            Map<String, Object> attributes = annotationMetadata.getAnnotationAttributes(EnableRocketMQ.class.getName());
            Boolean producer = (Boolean) attributes.get("producerInit");
            if(producer != null && producer) {
                initProducer(annotationMetadata,beanDefinitionRegistry);
            }
            Boolean consumer = (Boolean) attributes.get("consumerInit");
            if(consumer != null && consumer) {
                initConsumer(annotationMetadata,beanDefinitionRegistry);
            }
        }



    }

    /**
     * 加载生产者
     * @param annotationMetadata
     * @param beanDefinitionRegistry
     */
    private void initProducer(AnnotationMetadata annotationMetadata, BeanDefinitionRegistry beanDefinitionRegistry) {
        BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.rootBeanDefinition(ProducerAutoConfiguration.class);
        beanDefinitionRegistry.registerBeanDefinition(ProducerAutoConfiguration.class.getName(),beanDefinitionBuilder.getBeanDefinition());
        logger.info("==================RocketMQ生产者实例已启动===========================");
    }

    /**
     * 加载消费者
     * @param annotationMetadata
     * @param beanDefinitionRegistry
     */
    private void initConsumer(AnnotationMetadata annotationMetadata, BeanDefinitionRegistry beanDefinitionRegistry) {
        BeanDefinitionBuilder beanDefinitionBuilder = BeanDefinitionBuilder.rootBeanDefinition(ConsumerPostProcessor.class);
        beanDefinitionRegistry.registerBeanDefinition(ConsumerPostProcessor.class.getName(),beanDefinitionBuilder.getBeanDefinition());
        logger.info("==================RocketMQ消费者处理器加载完毕===========================");
    }


}
