package com.sunhf.rocketmq.base;

import com.alibaba.fastjson.JSON;
import com.sunhf.rocketmq.exception.ObjectParserException;
import org.apache.rocketmq.common.message.MessageExt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * RocketMQ消费者抽象类
 * @param <T>
 */
abstract class AbstractMQConsumer<T> {

    private static final Logger logger = LoggerFactory.getLogger(AbstractMQConsumer.class);

    private static final Map<String,Class> wrapperDict;
    static {
        wrapperDict = new HashMap<>();
        wrapperDict.put(String.class.getName(), String.class);
        wrapperDict.put(Double.class.getName(), String.class);
        wrapperDict.put(Float.class.getName(), String.class);
        wrapperDict.put(Long.class.getName(), String.class);
        wrapperDict.put(Short.class.getName(), String.class);
        wrapperDict.put(Character.class.getName(), String.class);
        wrapperDict.put(Byte.class.getName(), String.class);
        wrapperDict.put(Integer.class.getName(), String.class);
    }

    /**
     * 反序列化解析消息
     *
     * @param message  消息体
     * @return 序列化结果
     */
    protected T parseMessage(MessageExt message) {
        if (message == null || message.getBody() == null) {
            return null;
        }
        final Type type = this.getMessageType();

        if (type instanceof Class) {
            if(wrapperDict.containsKey(type.getTypeName())) {
                return parseWrapperObject(message.getBody(), wrapperDict.get(type.getTypeName()));
            } else {
                T data = JSON.parseObject(new String(message.getBody()), type);
                return data;
            }

        } else {
            logger.warn("消息反序列化失败. {}", message);
        }
        return null;
    }

    /**
     * 转换基本封装类型
     * @param messageBody
     * @return
     */
    private T parseWrapperObject(byte[] messageBody, Class clazz) {
        String body = new String(messageBody);
        if(clazz == String.class) {
            return (T) body;
        }
        throw new ObjectParserException("类型："+clazz.getName()+"包装类转换失败，不支持的类型");
    }



    protected Map<String, Object> parseExtParam(MessageExt message) {
        Map<String, Object> extMap = new HashMap<>();

        // parse message property
        extMap.put(MessageExtConst.PROPERTY_TOPIC, message.getTopic());
        extMap.putAll(message.getProperties());

        // parse messageExt property
        extMap.put(MessageExtConst.PROPERTY_EXT_BORN_HOST, message.getBornHost());
        extMap.put(MessageExtConst.PROPERTY_EXT_BORN_TIMESTAMP, message.getBornTimestamp());
        extMap.put(MessageExtConst.PROPERTY_EXT_COMMIT_LOG_OFFSET, message.getCommitLogOffset());
        extMap.put(MessageExtConst.PROPERTY_EXT_MSG_ID, message.getMsgId());
        extMap.put(MessageExtConst.PROPERTY_EXT_PREPARED_TRANSACTION_OFFSET, message.getPreparedTransactionOffset());
        extMap.put(MessageExtConst.PROPERTY_EXT_QUEUE_ID, message.getQueueId());
        extMap.put(MessageExtConst.PROPERTY_EXT_QUEUE_OFFSET, message.getQueueOffset());
        extMap.put(MessageExtConst.PROPERTY_EXT_RECONSUME_TIMES, message.getReconsumeTimes());
        extMap.put(MessageExtConst.PROPERTY_EXT_STORE_HOST, message.getStoreHost());
        extMap.put(MessageExtConst.PROPERTY_EXT_STORE_SIZE, message.getStoreSize());
        extMap.put(MessageExtConst.PROPERTY_EXT_STORE_TIMESTAMP, message.getStoreTimestamp());
        extMap.put(MessageExtConst.PROPERTY_EXT_SYS_FLAG, message.getSysFlag());
        extMap.put(MessageExtConst.PROPERTY_EXT_BODY_CRC, message.getBodyCRC());

        return extMap;
    }

    /**
     * 解析消息类型
     *
     * @return 消息类型
     */
    protected Type getMessageType() {
        Type superType = this.getClass().getGenericSuperclass();
        if (superType instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) superType;
            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
            Assert.isTrue(actualTypeArguments.length == 1, "Number of type arguments must be 1");
            return actualTypeArguments[0];
        } else {
            // 如果没有定义泛型，解析为Object
            return Object.class;
        }
    }
}
