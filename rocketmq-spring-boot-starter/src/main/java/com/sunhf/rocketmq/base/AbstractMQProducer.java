package com.sunhf.rocketmq.base;

import com.sunhf.rocketmq.exception.RocketMQException;
import org.apache.commons.lang3.StringUtils;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.MessageQueueSelector;
import org.apache.rocketmq.client.producer.SendCallback;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.client.producer.selector.SelectMessageQueueByHash;
import org.apache.rocketmq.common.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * RocketMQ的生产者的抽象类
 */
abstract class AbstractMQProducer {

    private static final Logger logger = LoggerFactory.getLogger(AbstractMQProducer.class);

    private static MessageQueueSelector messageQueueSelector = new SelectMessageQueueByHash();

    @Autowired
    private DefaultMQProducer producer;

    /**
     * 同步发送消息
     * @param message  消息体
     */
    public void syncSend(Message message)  {
        try {
            SendResult sendResult = producer.send(message);
            logger.debug("send rocketmq message ,messageId : {}", sendResult.getMsgId());
            this.doAfterSyncSend(message, sendResult);
        } catch (Exception e) {
            logger.error("消息发送失败，topic : {}, msgObj {}", message.getTopic(), message);
            throw new RocketMQException("消息发送失败，topic :" + message.getTopic() + ",e:" + e.getMessage());
        }
    }


    /**
     * 同步发送消息
     * @param message  消息体
     * @param hashKey  用于hash后选择queue的key
     */
    public void syncSendOrderly(Message message, String hashKey)  {
        if(StringUtils.isEmpty(hashKey)) {
            // fall back to normal
            syncSend(message);
        }
        try {
            SendResult sendResult = producer.send(message, messageQueueSelector, hashKey);
            logger.debug("send rocketmq message orderly ,messageId : {}", sendResult.getMsgId());
            this.doAfterSyncSend(message, sendResult);
        } catch (Exception e) {
            logger.error("顺序消息发送失败，topic : {}, msgObj {}", message.getTopic(), message);
            throw new RocketMQException("顺序消息发送失败，topic :" + message.getTopic() + ",e:" + e.getMessage());
        }
    }

    /**
     * 重写此方法处理发送后的逻辑
     * @param message  发送消息体
     * @param sendResult  发送结果
     */
    abstract void doAfterSyncSend(Message message, SendResult sendResult);

    /**
     * 异步发送消息
     * @param message msgObj
     * @param sendCallback 回调
     */
    public void asyncSend(Message message, SendCallback sendCallback) {
        try {
            producer.send(message, sendCallback);
            logger.debug("send rocketmq message async");
        } catch (Exception e) {
            logger.error("消息发送失败，topic : {}, msgObj {}", message.getTopic(), message);
            throw new RocketMQException("消息发送失败，topic :" + message.getTopic() + ",e:" + e.getMessage());
        }
    }
}
