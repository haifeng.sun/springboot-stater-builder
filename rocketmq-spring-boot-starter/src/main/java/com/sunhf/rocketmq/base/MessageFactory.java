package com.sunhf.rocketmq.base;


import com.alibaba.fastjson.JSON;
import com.sunhf.rocketmq.enums.DelayTimeLevel;
import org.apache.commons.lang3.StringUtils;
import org.apache.rocketmq.common.message.Message;

import java.nio.charset.Charset;

public class MessageFactory {


    private static final String[] DELAY_ARRAY = "1s 5s 10s 30s 1m 2m 3m 4m 5m 6m 7m 8m 9m 10m 20m 30m 1h 2h".split(" ");

    private String topic;
    private String tag;
    private String key;
    private Integer delayTimeLevel;


    public static MessageFactory getInstance(String topic, String tag) {
        return getInstance(topic, tag, null);
    }

    public static MessageFactory getInstance(String topic, String tag, DelayTimeLevel delayTimeLevel) {
        MessageFactory template = new MessageFactory();
        template.setTopic(topic);
        template.setTag(tag);
        if(delayTimeLevel != null) {
            template.setDelayTimeLevel(delayTimeLevel.getLevel());
        }
        return template;
    }


    public Message create(Object msgBean) {
        String messageKey= "";

        String str = JSON.toJSONString(msgBean);
        if(StringUtils.isEmpty(topic)) {
            if(StringUtils.isEmpty(getTopic())) {
                throw new RuntimeException("no topic defined to send this message");
            }
        }
        Message message = new Message(topic, str.getBytes(Charset.forName("utf-8")));
        if (!StringUtils.isEmpty(tag)) {
            message.setTags(tag);
        }
        if(StringUtils.isNotEmpty(messageKey)) {
            message.setKeys(messageKey);
        }
        if(delayTimeLevel != null && delayTimeLevel > 0 && delayTimeLevel <= DELAY_ARRAY.length) {
            message.setDelayTimeLevel(delayTimeLevel);
        }
        return message;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Integer getDelayTimeLevel() {
        return delayTimeLevel;
    }

    public void setDelayTimeLevel(Integer delayTimeLevel) {
        this.delayTimeLevel = delayTimeLevel;
    }
}
