package com.sunhf.rocketmq.config;

import com.sunhf.rocketmq.annotation.Consumer;
import com.sunhf.rocketmq.base.AbstractMQPushConsumer;
import com.sunhf.rocketmq.exception.RocketMQException;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerOrderly;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.Assert;

import java.util.List;
import java.util.UUID;

/**
 * 加载bean前后的处理过程
 */
public class ConsumerPostProcessor implements BeanPostProcessor {

    private static final Logger logger = LoggerFactory.getLogger(ConsumerPostProcessor.class);


    @Autowired
    private RocketMQProperties properties;

    @Override
    public Object postProcessBeforeInitialization(Object bean, String s) throws BeansException {
        Consumer annotation = AnnotationUtils.findAnnotation(bean.getClass(), Consumer.class);
        if(annotation != null) {
           publishConsumer(bean);
        }
        return bean;
    }

    private void publishConsumer(Object bean) {
        Consumer annotation = AnnotationUtils.findAnnotation(bean.getClass(), Consumer.class);
        if (properties.getNamesrvAddr() == null || properties.getNamesrvAddr().isEmpty()) {
            throw new RuntimeException("name server address must be defined");
        }
        Assert.notNull(annotation.consumerGroup(), "consumer's consumerGroup must be defined");
        Assert.notNull(annotation.topic(), "consumer's topic must be defined");
        if (!AbstractMQPushConsumer.class.isAssignableFrom(bean.getClass())) {
            throw new RocketMQException(bean.getClass().getName() + " - consumer未实现Consumer抽象类");
        }

        String consumerGroup = annotation.consumerGroup();
        String topic = annotation.topic();

        // 配置push consumer
        if (AbstractMQPushConsumer.class.isAssignableFrom(bean.getClass())) {
            DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(consumerGroup);
            consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET);
            consumer.setNamesrvAddr(properties.getNamesrvAddr());
            try {
                consumer.subscribe(topic, annotation.tag());
            } catch (MQClientException e) {
                throw new RocketMQException("消费者创建失败，信息：", e);
            }
            consumer.setInstanceName(UUID.randomUUID().toString());
            consumer.setVipChannelEnabled(properties.getVipChannelEnabled());
            AbstractMQPushConsumer abstractMQPushConsumer = (AbstractMQPushConsumer) bean;
            consumer.registerMessageListener(new MessageListenerOrderly() {
                @Override
                public ConsumeOrderlyStatus consumeMessage(List<MessageExt> msgs, ConsumeOrderlyContext context) {
                    return abstractMQPushConsumer.dealMessage(msgs, context);
                }
            });
            abstractMQPushConsumer.setConsumer(consumer);

            try {
                consumer.start();
                logger.info(String.format("%s 消费者启动成功，已进入就绪状态", bean.getClass().getName()));
            } catch (MQClientException e) {
                throw new RocketMQException(bean.getClass().getName() +" 消费者启动失败，信息：", e);
            }
        }


    }


    private DefaultMQPushConsumer initConsumer(String group, String topic, String tag) {
        if(group == null || group.isEmpty() || topic == null || topic.isEmpty()) throw new IllegalArgumentException("topic名称不能为空");
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(group);
        consumer.setNamesrvAddr(properties.getNamesrvAddr());
        try {
            consumer.subscribe(topic, tag);
        } catch (MQClientException e) {
            throw new RocketMQException("消费者创建失败,异常信息:错误码："+e.getResponseCode()+",消息："+ e.getErrorMessage(), e);
        }
        return consumer;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String s) throws BeansException {
        //nothing todo
        return bean;
    }


}
