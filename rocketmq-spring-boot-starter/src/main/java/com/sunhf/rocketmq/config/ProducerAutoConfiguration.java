package com.sunhf.rocketmq.config;

import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;

public class ProducerAutoConfiguration extends RocketMQAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public DefaultMQProducer defaultMQProducer() throws MQClientException {
        DefaultMQProducer producer = new DefaultMQProducer(properties.getDefaultGroup());
        producer.setNamesrvAddr(properties.getNamesrvAddr());
        producer.setSendMsgTimeout(properties.getSendMsgTimeout());
        producer.setSendMessageWithVIPChannel(properties.getVipChannelEnabled());
        producer.start();
        return producer;
    }


}
