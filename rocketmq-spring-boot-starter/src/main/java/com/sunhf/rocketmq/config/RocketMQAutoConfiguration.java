package com.sunhf.rocketmq.config;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties(RocketMQProperties.class)
public class RocketMQAutoConfiguration {

    @Autowired
    RocketMQProperties properties;


}
