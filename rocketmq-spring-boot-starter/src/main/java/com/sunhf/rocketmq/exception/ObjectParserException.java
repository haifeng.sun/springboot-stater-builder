package com.sunhf.rocketmq.exception;

public class ObjectParserException extends RuntimeException {

    public ObjectParserException() {
        super();
    }

    public ObjectParserException(String message) {
        super(message);
    }

    public ObjectParserException(String message, Throwable cause) {
        super(message, cause);
    }

    public ObjectParserException(Throwable cause) {
        super(cause);
    }

    protected ObjectParserException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
