#WebClient Springboot Starter
##描述
参考fegin设计思想封装的基于Webclient的http远程调用工具包。
##Why WebClient
webclient是webflux提供的一个异步http调用工具，它与常规的RestTemplate相比有着良好的线程控制优势
例如高并发场景下。如果对resttemplate做了异步调用（Non-Blocking），则会产生大量线程
而Webclient则可以利用其特性，使用较少的线程资源来支持高并发访问。
##特性
- 基于webclient的异步调用，完美适配webflux
- 失败重试机制
- 异常处理机制
- 负载均衡【后期】
- 整合hystrix实现限流熔断降级[后期]
- 配置url后就不走负载均衡【后期】
##特点
- 依旧使用官方注解进行接口编写（除WebClient声明注解），目的是未来官方推出支持可进行平滑切换
- 参考现有fegin使用风格，降低学习成本
- 低耦合设计，为未来复杂业务场景使用Hash负载均衡提供支持
- 负载均衡实现后可支持多注册中心的场景
##进度
- 基本框架搭建完成
- 生成webclient方法未实现
- 隔离application命名空间未实现
##参考
- springcloud openfegin
- Webflux Webclient