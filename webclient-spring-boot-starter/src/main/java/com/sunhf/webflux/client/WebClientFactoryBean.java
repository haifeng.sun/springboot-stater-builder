package com.sunhf.webflux.client;

import com.sunhf.webflux.client.common.MethodInfo;
import com.sunhf.webflux.client.config.WebClientProperties;
import com.sunhf.webflux.client.handler.DefaultRequestHandler;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.HashMap;
import java.util.Map;

/**
 * WebClient代理工厂
 */
public class WebClientFactoryBean implements FactoryBean<Object>, InitializingBean,
        ApplicationContextAware {

    private Class<?> type;

    private Map<String, Object> attributes; //webclient注解相关信息

    private String baseUrl; //接口类RequestMapping上的路径

    private ApplicationContext applicationContext;

    /**
     * 该返回值为最终创建的bean
     * @return
     * @throws Exception
     */
    @Override
    public Object getObject() throws Exception {
        return  Proxy.newProxyInstance(this.getClass().getClassLoader(), new Class[]{type}, new InvocationHandler() {
            DefaultRequestHandler handler = new DefaultRequestHandler();
            //真正处理请求的方法
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                return handler.invoke(extractMethodInfo(method, args));
            }


        });

    }


    private MethodInfo extractMethodInfo(Method method, Object[] args) {
        MethodInfo methodInfo = new MethodInfo();
        //生成url
        extractUrl(method, methodInfo);
        //生成请求参数
        extractRequestBody(method, methodInfo, args);
        //获取返回值
        extractReturnInfo(method, methodInfo);
        return methodInfo;
    }

    private void extractReturnInfo(Method method, MethodInfo methodInfo) {
        // 返回flux还是mono
        // isAssignableFrom 判断类型是否某个的子类
        // instanceof 判断实例是否某个的子类
        boolean isFlux = method.getReturnType()
                .isAssignableFrom(Flux.class);
        methodInfo.setReturnFlux(isFlux);

        // 得到返回对象的实际类型
        Class<?> elementType = extractElementType(
                method.getGenericReturnType());
        methodInfo.setReturnElementType(elementType);
    }

    /**
     * 创建请求体
     * @param method
     * @param methodInfo
     */
    private void extractRequestBody(Method method, MethodInfo methodInfo, Object[] args) {
        // 得到调用的参数和body
        Parameter[] parameters = method.getParameters();

        // 参数和值对应的map
        Map<String, Object> params = new HashMap<>();
        methodInfo.setParams(params);

        for (int i = 0; i < parameters.length; i++) {
            // 是否带 @PathVariable
            PathVariable annoPath = parameters[i]
                    .getAnnotation(PathVariable.class);

            if (annoPath != null) {
                params.put(annoPath.value(), args[i]);
            }

            // 是否带了 RequestBody
            RequestBody annoBody = parameters[i]
                    .getAnnotation(RequestBody.class);

            if (annoBody != null) {
                methodInfo.setBody((Mono<?>) args[i]);
                // 请求对象的实际类型
                methodInfo.setBodyElementType(
                        extractElementType(parameters[i]
                                .getParameterizedType()));
            }
        }
    }

    /**
     * 得到泛型中的实际类型
     * @param parameterizedType
     * @return
     */
    private Class<?> extractElementType(Type parameterizedType) {
        Type[] actualTypeArguments = ((ParameterizedType) parameterizedType)
                .getActualTypeArguments();

        return (Class<?>) actualTypeArguments[0];
    }

    /**
     * 待完成内容：查找class上的RequestMapping，生成baseUrl
     * 识别方法上的RequestMapping， 默认检查是否指定请求类型 默认发送请求
     * @param method
     * @param methodInfo
     */
    private void extractUrl(Method method, MethodInfo methodInfo) {
        WebClientProperties config = applicationContext.getBean(WebClientProperties.class);

        // 得到请求URL和请求方法
        Annotation[] annotations = method.getAnnotations();
        StringBuilder urlBuilder = new StringBuilder();
        if(config.isHttps()) {
            urlBuilder.append("https://");
        } else {
            urlBuilder.append("http://");
        }

        for (Annotation annotation : annotations) {
            // GET
            if (annotation instanceof GetMapping) {
                GetMapping anno = (GetMapping) annotation;
                contractUrl(urlBuilder, anno.value()[0]);
                methodInfo.setUrl(urlBuilder.toString());
                methodInfo.setMethod(HttpMethod.GET);
            }
            // POST
            else if (annotation instanceof PostMapping) {
                PostMapping anno = (PostMapping) annotation;
                contractUrl(urlBuilder, anno.value()[0]);
                methodInfo.setUrl(urlBuilder.toString());
                methodInfo.setMethod(HttpMethod.POST);
            }
            //PUT
            else if(annotation instanceof PutMapping) {
                PutMapping anno = (PutMapping) annotation;
                contractUrl(urlBuilder, anno.value()[0]);
                methodInfo.setUrl(urlBuilder.toString());
                methodInfo.setMethod(HttpMethod.PUT);
            }
            // DELETE
            else if (annotation instanceof DeleteMapping) {
                DeleteMapping anno = (DeleteMapping) annotation;
                contractUrl(urlBuilder, anno.value()[0]);
                methodInfo.setUrl(urlBuilder.toString());
                methodInfo.setMethod(HttpMethod.DELETE);
            } else if(annotation instanceof RequestMapping) {
                RequestMapping anno = (RequestMapping) annotation;
                RequestMethod[] methods = anno.method();
                if(method!=null && methods.length > 0) {
                    RequestMethod m = methods[0]; //请求一次只能发一个，所以只匹配第一个类型，不填默认发post
                    contractUrl(urlBuilder, anno.value()[0]);
                    methodInfo.setUrl(urlBuilder.toString());
                    switch (m) {
                        case POST:
                            methodInfo.setMethod(HttpMethod.POST); break;
                        case GET:
                            methodInfo.setMethod(HttpMethod.GET); break;
                        case DELETE:
                            methodInfo.setMethod(HttpMethod.DELETE); break;
                        case PUT:
                            methodInfo.setMethod(HttpMethod.PUT); break;
                        default:
                            methodInfo.setMethod(HttpMethod.POST);
                    }

                } else {
                    methodInfo.setMethod(HttpMethod.POST);
                }
                contractUrl(urlBuilder, anno.value()[0]);
                methodInfo.setUrl(urlBuilder.toString());
            }

        }

    }

    private void contractUrl(StringBuilder urlBuilder, String s) {
        if(s ==null || s.isEmpty() || "/".equals(s)) return;
        if(urlBuilder.lastIndexOf("/") != urlBuilder.length() && s.charAt(0) != '/') {
            urlBuilder.append("/");
        }
        urlBuilder.append(s);
    }


    @Override
    public Class<?> getObjectType() {
        return type;
    }

    @Override
    public void afterPropertiesSet() throws Exception {

    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
