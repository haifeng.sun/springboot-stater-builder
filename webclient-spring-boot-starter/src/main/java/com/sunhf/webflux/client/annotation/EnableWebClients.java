package com.sunhf.webflux.client.annotation;

import com.sunhf.webflux.client.WebClientRegistrar;
import com.sunhf.webflux.client.config.WebClientAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({WebClientRegistrar.class, WebClientAutoConfiguration.class})
public @interface EnableWebClients {

    String[] basePackages() default {};


}
