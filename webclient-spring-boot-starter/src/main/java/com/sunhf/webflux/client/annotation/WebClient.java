package com.sunhf.webflux.client.annotation;

import com.sunhf.webflux.client.WebClientRegistrar;
import com.sunhf.webflux.client.config.WebClientAutoConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WebClient {

    String appName(); //服务名称
}
