package com.sunhf.webflux.client.common;

import lombok.Data;
import org.springframework.http.HttpMethod;
import reactor.core.publisher.Mono;

import java.util.Map;

@Data
public class MethodInfo {

    private String url; //url
    private HttpMethod method; //请求类型
    private Map<String, Object> params; //请求参数
    private Mono body;
    private Class<?> bodyElementType;
    private boolean returnFlux;//返回是flux还是mono
    private Class<?> returnElementType;//返回对象的类型

}
