package com.sunhf.webflux.client.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "spring.webclient")
public class WebClientProperties {

    private String defaultZone; //注册中心地址

    private boolean https = false; //是否启用https
}
