package com.sunhf.webflux.client.handler;

import com.sunhf.webflux.client.common.MethodInfo;

/**
 * 请求处理器
 */
public interface RequestHandler<T> {

    /**
     * 处理请求
     * @param info
     * @return
     */
    T invoke(MethodInfo info);
}
