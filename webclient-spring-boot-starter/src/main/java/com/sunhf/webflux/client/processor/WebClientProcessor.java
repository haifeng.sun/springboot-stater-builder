package com.sunhf.webflux.client.processor;

import com.sunhf.webflux.client.annotation.WebClient;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.core.annotation.AnnotationUtils;

public class WebClientProcessor implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        WebClient annoWebclient = AnnotationUtils.findAnnotation(bean.getClass(), WebClient.class);
        String appName = annoWebclient.appName();
        if(appName != null && !appName.isEmpty()) {

        }
        return null;
    }


    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        return null;
    }
}
