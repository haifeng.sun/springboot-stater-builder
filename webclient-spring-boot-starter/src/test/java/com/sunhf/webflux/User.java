package com.sunhf.webflux;

import lombok.Data;

@Data
public class User {

    private Long id;
    private String username;
}
