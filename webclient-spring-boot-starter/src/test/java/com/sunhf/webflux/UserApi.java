package com.sunhf.webflux;

import com.sunhf.webflux.client.annotation.WebClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@WebClient("TEST_WEBCLIENT_PROJECT")
@RequestMapping("/user")
public interface UserApi {

    @GetMapping("/getUser/{userId}")
    User getUser(@PathVariable("userId") String userId);
}
