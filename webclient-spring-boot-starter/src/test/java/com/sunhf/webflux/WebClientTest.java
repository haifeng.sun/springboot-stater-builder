package com.sunhf.webflux;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class WebClientTest {

    @Autowired
    private UserApi userApi;

    @Test
    public void test() {
       User u = userApi.getUser("1");
       Assert.assertNotNull(u);
    }

}
